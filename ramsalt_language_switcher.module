<?php

/**
 * @file
 * Contains ramsalt_language_switcher.module.
 *
 * Note: This is based on:
 * https://www.drupal.org/node/2791231#comment-12004615
 * https://blog.liip.ch/archive/2017/04/12/drupal-8-multilanguage-improvements.html
 *
 * Since the developer has not implemented the module yet, we have it custom here
 * and we can discard when the module gets.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;


/**
 * Implements hook_help().
 */
function ramsalt_language_switcher_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the ramsalt_language_switcher module.
    case 'help.page.ramsalt_language_switcher':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Disables links to translations that do not exist.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_language_switch_links_alter().
 */
function ramsalt_language_switcher_language_switch_links_alter(array &$links, $type, $path) {
  $entity = _ramsalt_language_switcher_get_page_entity();
  if ($entity) {
    $new_links = array();
    foreach ($links as $lang_code => $link) {
      try {
        $translation = $entity->getTranslation($lang_code)->access('view');
        if ($translation) {
          $new_links[$lang_code] = $link;
        }
      }
      catch (\InvalidArgumentException $e) {
        // No translation for that language, so handle properly.

        // Add a class
        $link['attributes']['class'][] = 'translation-unavailable';

        // Ask from other modules to do their own changes.
        $context = array(
          'entity' => clone $entity,
        );
        \Drupal::moduleHandler()->alter('ramsalt_language_switcher_link', $link, $context);

        $new_links[$lang_code] = $link;
      }
    }
    $links = $new_links;
  }
}

/**
 * Retrieve the current page entity.
 *
 * @return bool|\Drupal\Core\Entity\ContentEntityInterface
 *   The retrieved entity, or FALSE if none found.
 */
function _ramsalt_language_switcher_get_page_entity() {
  $params = \Drupal::routeMatch()->getParameters()->all();

  foreach( $params as $param ) {
    if ($param instanceof ContentEntityInterface) {
      return $param;
    }
  }
  return FALSE;
}
