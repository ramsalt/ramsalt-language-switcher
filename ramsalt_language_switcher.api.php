<?php

/**
 * @file
 * Hooks related to ramsalt_language_switcher.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the language link renderable element, provided in ramsalt_language_switcher.
 *
 * @param $link
 *   Render array of the link that points to the translated version of the content
 *   entity.
 * @param $context
 *   Array, contains the content entity under the 'entity' element.
 */
function hook_ramsalt_language_switcher_link_alter(&$link, $context) {
  $link['attributes']['class'][] = 'add-another-class';
}

/**
 * @} End of "addtogroup hooks".
 */
